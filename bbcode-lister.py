#!/usr/bin/env python

import sys
import os
import mutagen
import glob

read_directory = "." if len(sys.argv) == 1 else sys.argv[1]
supported_file_types = ('*.mp3', '*.flac')
supported_files = []

os.chdir(read_directory)

# Get a list of files that are supported
for files in supported_file_types:
	supported_files.extend(glob.glob(files))

# Exit the script if no supported files found
if len(supported_files) == 0:
	print("No usable files found!")
	exit()

# Get title line
if supported_files[0].endswith('.mp3'):
	track = mutagen.File(os.path.abspath(supported_files[0]), easy=True)
	artist = track["performer"][0]
elif supported_files[0].endswith('.flac'):
	track = mutagen.File(os.path.abspath(supported_files[0]), easy=True)
	artist = track["albumartist"][0]

album = track["album"][0]
year = track["date"][0]

if artist == str.lower("various artists"):
	print("[size=4]Various Artists - " + album + " (" + year + ")[/size]")
else:
	print("[size=4]" + artist + " - " + album + " (" + year + ")[/size]")

for f in supported_files:
    if f.endswith('.mp3') or f.endswith('.flac'):
		track = mutagen.File(os.path.abspath(f), easy=True)

		artist = track["artist"][0]
		album = track["album"][0]
		title = track["title"][0]
		year = track["date"][0]
		track_no = track["tracknumber"][0].split("/")[0]
		track_time = track.info.length
		track_min = str(int(track_time / 60))
		track_sec = str(int(track_time % 60)).zfill(2)
		hr_time = track_min + ":" + track_sec        
		print("[#]" + artist + " - " + title + " (" + hr_time + ")")

